<?php

require 'vendor/autoload.php';
require_once 'Actions.php';

$actions = new Actions();
$actions->wipeConnection();

$socket = new React\Socket\SocketServer('tcp://10.1.1.13:9006');
$socket = new React\Socket\LimitingServer($socket, 400);

$socket->on('connection', function (React\Socket\ConnectionInterface $connection) {
    $actions = new Actions();
    $actions->saveConnection($connection->getRemoteAddress());
    
    $connection->on('data', function ($data) use ($connection) {
        $actions = new Actions();
        // Atualiza a conexão
        $actions->updateConnection($connection->getRemoteAddress(), $data);
        $actions->addString($data);
        // Checa comandos pendentes
        if ($command = $actions->getCommands($data)) {
            // Envia o comando
            $connection->write($command['comando_string']);
            // Atualiza o comando pendente
            $actions->updateRow($command['ID_Disp'], "data_envio");
        }
    });

    $connection->on('close', function () use ($connection) {
        $actions = new Actions();
        $actions->removeConnection($connection->getRemoteAddress());
    });

});

$socket->on('error', function (Exception $e) {
    echo 'Error: ' . $e->getMessage() . PHP_EOL;
});

echo 'Suntech Gateway | Listening on ' . strtr($socket->getAddress(), array('tcp:' => 'http:', 'tls:' => 'https:')) . PHP_EOL;
