<?php

class Actions
{
	private $conn;

	public function getConnection() 
	{
		$servername = "localhost:3345";
		$username = "flz67";
		$password = 'desenvolvimento@12';
		$database = "suntech_gateway";

		$this->conn = new mysqli($servername, $username, $password, $database);

		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
			return;
		}
	}

	public function addString($string)
	{
		$this->getConnection();

		if (! $string) return;

		$string = $this->filterString($string);

		if (! $string) {
			return false;
		}

		$sql = "INSERT INTO fila_i0 (fila_dadobruto) VALUES ('".$string."')";

		$this->conn->query($sql);
		$this->conn->close();

		$serial = $this->getSerial($string);

		if ($serial == 0) {
			$serial = $this->getSerialRes($string);
			$this->updateRow($serial, "data_confirmacao");
			$this->saveRow($serial);
		}

		return;
	}

	private function filterString($string)
	{
		if (substr_count($string, 'ST300STT') == 1 || 
            substr_count($string, 'ST300CMD') == 1 ||
        	substr_count($string, 'ST910') == 1) {
			return trim($string);
		}

		return false;
	}

	private function getSerial($string)
	{
		$string = explode(";", $string);
		return intval($string[1]);
	}

	private function getSerialRes($string)
	{
		$string = explode(";", $string);
		return intval($string[2]);
	}

	public function getCommands($string)
	{
		$this->getConnection();

		$string = $this->filterString($string);

		if (! $string) {
			return false;
		}

		$serial = $this->getSerial($string);

		if (! $serial || $serial == 0) {
			return false;
		}

		$sql = "SELECT * FROM comando_pendente WHERE ID_Disp = " . $serial . " AND data_envio IS NULL LIMIT 1";
		$query = $this->conn->query($sql);
		$this->conn->close();

		if ($query->num_rows > 0) {
			return $query->fetch_assoc();
		}

		return false;
	} 

	public function updateRow($serial, $cell)
	{
		$this->getConnection();

		if (! $serial) {
			return false;
		}

		$sql = "UPDATE comando_pendente SET ".$cell." = '".date('Y-m-d H:i:s')."' WHERE ID_Disp = ".$serial." AND ISNULL(".$cell.")";
		$query = $this->conn->query($sql);
		$this->conn->close();

		if (!! $query) {
			return true;
		}

		return false;
	}

	private function saveRow($serial)
	{
		$this->getConnection();

		if (! $serial) {
			return false;
		}

		$getSql = "SELECT * FROM comando_pendente WHERE ID_Disp = " . $serial;
		$getQuery = $this->conn->query($getSql);

		$data = $getQuery->fetch_assoc();

		if (! $data) {
			return false;
		}

		if (! $data['usuario']) {
			$data['usuario'] = 'null';
		}

		$sql = "INSERT INTO comando_enviado (nsu_comando, ID_Disp, comando_nome, comando_string, data_solicitacao, data_envio, data_confirmacao, usuario, observacao) VALUES (".$data['nsu_comando'].", ".$data['ID_Disp'].", '".$data['comando_nome']."', '".trim($data['comando_string'])."', '".$data['data_solicitacao']."', '".$data['data_envio']."', '".$data['data_confirmacao']."', ".$data['usuario'].", '".$data['observacao']."')";

		$query = $this->conn->query($sql);
		$this->conn->close();

		return $query;
	}

	public function saveConnection($ip)
	{
		$this->getConnection();

		if (! $ip) { 
			return false;
		}

		$sqlConsulta = "SELECT * FROM connections WHERE ip = '".$ip."'";
		$queryCon = $this->conn->query($sqlConsulta);
		
		if ($queryCon->num_rows > 0) {
			return false;
		}

		$sql = "INSERT INTO connections (ip) VALUES ('".$ip."')";
		$query = $this->conn->query($sql);

		$this->conn->close();
	}

	public function updateConnection($ip, $string)
	{
		$this->getConnection();

		if (! $ip || ! $string) { 
			return false;
		}

		$serial = $this->getSerial($string);

		if (! $serial) {
			$serial = $this->getSerialRes($string);
			if (! $serial) {
				return false;
			}
		}

		$sql = "UPDATE connections SET `serial` = '".$serial."' WHERE `ip` = '".$ip."' AND `serial` IS NULL";
		$query = $this->conn->query($sql);
		$this->conn->close();

		if (!! $oldConnection = $this->verifyConnection($ip, $serial)) 
		{
			$old = explode(":", $oldConnection['ip']);
			$ip = str_replace('//', '', $old[1]);
			$port = $old[2];

			shell_exec('echo -e "43M*L7ey31#" | sudo -S ss -K dst '.$ip.' dport = '.$port.' &> /dev/null');
			$this->removeConnection($oldConnection['ip']);
		}

		return true;
	}

	public function removeConnection($ip)
	{
		$this->getConnection();

		if (! $ip) { 
			return false;
		}

		$sql = "DELETE FROM connections WHERE ip = '".$ip."'";
		$query = $this->conn->query($sql);
		$this->conn->close();
	}

	private function verifyConnection($ip, $serial)
	{
		$this->getConnection();

		if (! $ip || ! $serial) { 
			return false;
		}

		$sql = "SELECT * FROM connections WHERE `serial` = '".$serial."' AND ip != '".$ip."'";
		$query = $this->conn->query($sql);
		$this->conn->close();

		if ($query->num_rows > 0) {
			return $query->fetch_assoc();
		}

		return false;
	}

	public function wipeConnection()
	{
		$this->getConnection();

		$sql = "TRUNCATE TABLE connections";
		$query = $this->conn->query($sql);
		$this->conn->close();
	}

}